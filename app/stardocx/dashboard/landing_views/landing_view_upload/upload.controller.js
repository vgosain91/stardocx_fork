(function(){
  'use strict';
  function UploadController(uploadService,$state,$timeout){
    var vm = this;
    vm.doAction = function(){
      $state.reload();
    };
    vm.showUploadForm=false;


    vm.onFileLoad= function (){
      vm.showUploadForm=true;
    }

    vm.Cancel = function (){
      vm.showUploadForm =false;
    }
    vm.Publish = function(){
      if(vm.myfile){
        if(vm.myfile.filesize<5242880){
          vm.showUploadForm=false;
          vm.uploading = true;
          vm.uploaded = false;

          uploadService
          .uploadData(vm.myfile)
          .then(funcSuccess,funcError);

          function funcSuccess(response){
            var msg = "Successfully uploaded";
            vm.uploading = false;
            vm.uploaded = true;
            vm.open(msg);
          }

          function funcError(err){
            var msg="Your upload(s) failed.Kindly try after some time.";
            vm.open(msg);
            if(err.msg==="login invalid"){
              //show modal for asking user to login again.
              $state.go("user-login");
            }
          }
        }
        else{
          var msg="File with size greater than 5 MB is not allowed.";
          vm.open(msg);
        }
      }
    }
  }

  UploadController.$inject=['uploadService','$state','$timeout'];

  angular
  .module('app.dashboard')
  .controller('UploadController',UploadController);
})();
