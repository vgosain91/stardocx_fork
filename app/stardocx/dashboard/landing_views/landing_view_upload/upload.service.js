(function(){
'use strict';
function uploadService( $http, $q, DataStore, apiBaseUrl ){
  var service ={
    uploadData:__uploadData
  }
  function __uploadData(myfile){
    var deferred =$q.defer();

    //TODO : Vinay/Vatsal. Remove $.ajax. use angular methods instead.
    //This may not trigger angular digest cycle and will have to manage state changes ourself
    $http({
      url: apiBaseUrl + '/document',
      method:'POST',
      data:myfile,
    }).then(funcSuccess,funcError);

    function funcSuccess(response){
      deferred.resolve(response);
    }

    function funcError(err){
       deferred.reject({msg:err});
    }
    return deferred.promise;
  }
  return service;
}

uploadService.$inject=['$http','$q','DataStore','apiBaseUrl'];

  angular
    .module('app.dashboard')
    .factory('uploadService',uploadService);
})();
