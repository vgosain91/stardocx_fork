(function (){
  'use strict';
  function NoticeBoxController($state,noticeBoxService){
    var vm =this;
    var voteCase="";

    vm.goToDetail=function(){
      $state.go('notice-details',{ NID: vm.notice.id });
    }

    vm.date = noticeBoxService
    .getDate(vm.notice.updatedAt);

    vm.timeelapsed = noticeBoxService
    .getTimeElapsed(vm.notice.updatedAt);

    noticeBoxService
    .getVoteStatus(vm.notice.id)
    .then(funcSuccess);

    function funcSuccess(response){
    switch (response.data.type) {
      case "upVote":
          vm.notice.isUpvoted= true;
        break;
      case "downVote":
          vm.notice.isDownvoted=true;
    }
  }


    vm.upVote=function(){

      if(!vm.notice.isUpvoted){
        if(!vm.notice.isDownvoted){
          (vm.notice.upVotes)++;
          vm.notice.isUpvoted=true;
          voteCase="CaseOne";
        }
        else{
          (vm.notice.downVotes)--;
          vm.notice.isDownvoted=false;
          voteCase="CaseFour";
        }
      }
      else if(vm.notice.isUpvoted){
        (vm.notice.upVotes)--;
        vm.notice.isUpvoted=false;
        voteCase="CaseThree";
      }
      serviceCall(vm,"upVote");
    }

    vm.downVote = function(){
      if(!vm.notice.isDownvoted){
        if(!vm.notice.isUpvoted){
          (vm.notice.downVotes)++;
          vm.notice.isDownvoted=true;
          voteCase="CaseTwo";
        }
        else{
          (vm.notice.upVotes)--;
          vm.notice.isUpvoted=false;
          voteCase="CaseThree";
        }
      }
      else if(vm.notice.isDownvoted){
        (vm.notice.downVotes)--;
        vm.notice.isDownvoted=false;
        voteCase="CaseFour";
      }
      serviceCall(vm,"downVote");
    }

    function serviceCall(vm,voteType){
      noticeBoxService
      .Vote(vm,voteType)
      .then(funcSucc,funcErr);

      function funcSucc(){
        //TODO Think and handle success condition if any scenario left
      }

      function funcErr(){
        switch (voteCase) {
          case "CaseOne":
          vm.notice.isUpvoted= false;
          vm.notice.upVotes--;
          break;
          case "CaseTwo":
          vm.notice.isDownvoted =false;
          vm.notice.downVotes--;
          break;
          case "CaseThree":
          vm.notice.isUpvoted=true;
          vm.notice.upVotes++;
          break;
          case "CaseFour":
          vm.notice.isDownvoted=true;
          vm.notice.downVotes++;
        }
      }
    }
}

NoticeBoxController.$inject=['$state','noticeBoxService'];

angular
.module('app.dashboard')
.controller('NoticeBoxController',NoticeBoxController);
})();
