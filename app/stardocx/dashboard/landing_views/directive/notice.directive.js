(function(){
  'use strict';
  function noticeBox(){

    var directive= {
      scope:{},
      bindToController:{
        notice:'='
      },
      controller:'NoticeBoxController as nb',
      restrict:'E',
      templateUrl:'stardocx/dashboard/landing_views/directive/notice-box.html'
    };
    return directive;
  }

  angular
  .module('app.dashboard')
  .directive('noticeBox',noticeBox);
})();
