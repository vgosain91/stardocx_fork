(function (){
  'use strict';
  function noticeBoxService($http,apiBaseUrl,$q,DataStore){
    var service={
      getDate:__getDate,
      getTimeElapsed:__getTimeElapsed,
      getVoteStatus:__getVoteStatus,
      Vote:__Vote,
    }

    function __getDate(dateString){

      var date= new Date(dateString).toLocaleDateString('en-GB', {
        day : 'numeric',
        month : 'short',
        year : 'numeric'
      }).split(' ').join('-');

      return date;
    }

    function __getTimeElapsed(updatedAt){
      var updatedDate= Date.parse(updatedAt);
      var currentDate= Date.now();

      var elapsedTime= currentDate-updatedDate;
      elapsedTime = elapsedTime/1000;

      if(elapsedTime<60){
        return "0 minutes ago";
      }
      else if(elapsedTime>60 && elapsedTime<3600){
        elapsedTime = elapsedTime/60;
        elapsedTime=Math.trunc(elapsedTime);
        return  elapsedTime.toString() + ( (elapsedTime===1)?" minute ago":" minutes ago");
      }
      else if(elapsedTime>3600 && elapsedTime<86400){
        elapsedTime=elapsedTime/3600;
        elapsedTime=Math.trunc(elapsedTime);
        return  elapsedTime.toString() + ( (elapsedTime===1)?" hour ago":" hours ago");
      }
      else if(elapsedTime>86400){
        elapsedTime = elapsedTime/86400;
        elapsedTime=Math.trunc(elapsedTime);
        return  elapsedTime.toString() + ((elapsedTime===1)?" day ago":" days ago");
      }
      else{
        return "";
      }
    }

    function __getVoteStatus(nid){

      var userFbInfo =DataStore.get('userFbInfo');
      var deferred = $q.defer();

      $http({
        url: apiBaseUrl + '/vote/check/' +  nid + userFbInfo.id,
        method:'GET'
      }).then(funcSucc);

      function funcSucc(response){
        deferred.resolve(response);
      }
      return deferred.promise;
    }

    var permanentToken = DataStore.get('permanentToken');

    function __Vote(viewmodal,voteType){
       var deferred =$q.defer();
      $.ajax({
        url: apiBaseUrl + "/vote/Document/" + viewmodal.notice.id + "/" + voteType,
        type:"POST",
        headers:{
            Authorization: 'Bearer ' + permanentToken
        },
        success:funcSuccess,
        error:funcError
      });

      function funcSuccess(){
        deferred.resolve();
      }

      function funcError(err){
        deferred.reject();
      }
      return deferred.promise;
    }

    return service;
  }

  noticeBoxService.$inject=['$http','apiBaseUrl','$q','DataStore'];

  angular
  .module('app.dashboard')
  .factory('noticeBoxService',noticeBoxService);
})();
