(function(){
'use strict';
function profileService($http, $q, apiBaseUrl, DataStore){

  var service={
    getProfile : __getProfile,
    getUserUploads : __getUserUploads
  }

  function __getProfile(fbId){

    var deferred =$q.defer();

    $http({
      url:apiBaseUrl + '/user/?facebookId=' + fbId,
      method:'GET',
    }).then(funcSuccess,funcfailure);

    function funcSuccess(response){
      deferred.resolve(response.data);
    }

    function funcfailure(err){
      deferred.reject();
    }
    return deferred.promise;
  }


  function __getUserUploads(fbId){
    var deferred =$q.defer();

    var myLastDownload = DataStore.get('myLastDownload');
    if(myLastDownload){
     var timestamp = myLastDownload;
    }
    else{
      timestamp=0;
    }

    $http({
      url:apiBaseUrl + "/document/docs/" + timestamp + "/" + fbId,
      method:"GET",
    }).then(funcSuccess,funcfailure);

    function funcSuccess(response){

      DataStore.set('myLastDownload', Date.now());

      var myNotices = DataStore.get('mynotices');
      if(myNotices){
      myNotices.push.apply(myNotices,response.data);
      }
      else{
        DataStore.set('mynotices', response.data);
      }
      deferred.resolve();
    }

    function funcfailure(err){
      deferred.reject();
    }
    return deferred.promise;
  }
  return service;
}

profileService.$inject=['$http','$q','apiBaseUrl','DataStore'];

  angular
    .module('app.dashboard')
    .factory('profileService',profileService);
})();
