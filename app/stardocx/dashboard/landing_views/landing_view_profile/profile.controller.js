(function(){
  'use strict';
  function ProfileController(DataStore, profileService,homeService){
    var vm=this;
    if(DataStore.get("userFbInfo")){
      vm.userData = DataStore.get("userFbInfo")

      vm.userImageUrl = (vm.userData.picture && vm.userData.picture.data) ? vm.userData.picture.data['url'] : "";
      vm.userName= vm.userData.first_name+" "+vm.userData.last_name;
      vm.fbIdUrl="http://facebook.com/"+vm.userData.id;
      var notifications = DataStore.get("notifications");
      if(notifications){
        vm.notices =  notifications.filter(notificationFilter);
      }

      function notificationFilter(notice){
        if(notice.ownerName===vm.userName){
          return notice;
        }
      }

      profileService
      .getProfile(vm.userData.id)
      .then(funcSuccess,funcfailure);

      function funcSuccess(data){
        if(typeof data != 'undefined'){
          vm.userDetails=data[0];
        }
      }

      function funcfailure(){
      }

     profileService
      .getUserUploads(vm.userData.id)
      .then(funcSucc);

      function funcSucc(){
      vm.notices= DataStore.get("mynotices");
    }
  }
}

ProfileController.$inject=['DataStore','profileService','homeService'];

angular
.module('app.dashboard')
.controller('ProfileController',ProfileController);
})();
