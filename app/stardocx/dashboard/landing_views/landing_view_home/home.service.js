(function(){
  'use strict';
  function homeService($http,$q,DataStore,apiBaseUrl){
    var service={
      data:__fetchData
    };

    function __fetchData(){
      var deferred = $q.defer();
      var timestamp ="";
      var notifications = DataStore.get("notifications");
      var lastdownloadTimeStamp = DataStore.get("lastdownload");

      if( lastdownloadTimeStamp && notifications){
        timestamp = "docs/" + DataStore.get("lastdownload");
      }

      $http({
        url:apiBaseUrl+'/document/'+timestamp,
        type:'GET',
      }).then(funcSuccess,funcErr);

      function funcSuccess(response){
        DataStore.set("lastdownload", Date.now());

        if(notifications){
          notifications.push.apply(notifications,response.data);
        }
        else{
          DataStore.set("notifications", response.data);
        }
        deferred.resolve();
      }

      function funcErr(err){
        console.log(err);
        deferred.reject();
      }
      return deferred.promise;
    }
    return service;
  }

  homeService.$inject=['$http','$q','DataStore','apiBaseUrl'];
  angular
  .module('app.dashboard')
  .service('homeService',homeService);
})();
