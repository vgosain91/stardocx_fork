(function HomeIIFE(){
  'use strict';
  function HomeController(homeService,DataStore){
    var vm =this;
    vm.noticeCount =5;
    vm.collapse=false;

    var notifications = DataStore.get("notifications");

    if(notifications){
      vm.notices = notifications;
    }

    vm.incrementCount=function(){
      var notifications = DataStore.get("notifications");
      if(notifications && notifications.length !== 0){
        vm.noticeCount= notifications.length;
      }
      vm.collapse =true;
    }

    vm.decrementCount=function(){
      var notifications = DataStore.get("notifications");
      if(notifications && notifications!==0){
        vm.noticeCount=5;
      }
      vm.collapse=false;
    }

      homeService
      .data()
      .then(funcSucc);

      function funcSucc(){
        vm.notices=DataStore.get("notifications");
      }
    }


  HomeController.$inject=['homeService','DataStore'];
  angular
    .module('app.dashboard')
    .controller('HomeController',HomeController);
})();
