(function IIFE(){

  function noticeDetailsService($http,$q,DataStore,apiBaseUrl){

    var service ={
      getNotice :__getNotice
    }

    function __getNotice(NID){
      var deferred = $q.defer();
      var notices =DataStore.get('notifications');
      if(notices){
        var notice = notices.filter(noticeFilter);
        deferred.resolve(notice);
      }
      else{
        //notices not present ,user might have come from push notification or directly entering url.
        $http({
          url:apiBaseUrl + '/document/?id=' + NID,
          method:'GET',
        }).then(funcSuccess,funcError);

        function funcSuccess(response){
          var notice = response.data;
          deferred.resolve(notice);
        }

        function funcError(err){
          console.log(err)
          deferred.reject();
        }
      }

      //filter function
      function noticeFilter(notice){
      if(notice.id===NID)
      {
        return notice;
      }
      }
      return deferred.promise;
    }
    return service;
  }

  noticeDetailsService.$inject=['$http','$q','DataStore','apiBaseUrl'];
  angular
  .module('app.dashboard')
  .factory('NoticeDetailsService',noticeDetailsService);
})();
