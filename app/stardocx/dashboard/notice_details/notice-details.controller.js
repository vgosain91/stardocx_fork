(function(){
  'use strict';
  function NoticeDetailController($stateParams,$state,$location,DataStore,NoticeDetailsService){
    var vm =this;
    var noticeid = $stateParams.NID;

    NoticeDetailsService
    .getNotice(noticeid)
    .then(funcSuccess,funcError);

    function funcSuccess(notification){
      vm.notice =notification[0];
    }

    function funcError(){
      var msg = "OOPS ,Something went wrong. The notification you are looking for is unavailable at the moment .Kindly try again later."
      vm.open(msg);
    }

    vm.goBack=function(){
      $state.go('notices');
    }
    vm.noticeUrl= $location.absUrl();
  }

  NoticeDetailController.$inject=['$stateParams','$state','$location','DataStore','NoticeDetailsService'];

  angular
  .module('app.dashboard')
  .controller('NoticeDetailController',NoticeDetailController);

})();
