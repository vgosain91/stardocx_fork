(function(){
  'use strict';

  function configureRoutes($stateProvider,$urlRouterProvider){
    $urlRouterProvider.otherwise('/notices');

    $stateProvider
    .state('notices', {
      url: '/notices',
      templateUrl: 'stardocx/dashboard/dashboard.html'
    })

    .state('user-details', {
      url: '/user-details',
      templateUrl: 'stardocx/dashboard/user_details/user-details.html'
    })

    .state('notice-details', {
      url: '/notice-details/:NID',
      templateUrl: 'stardocx/dashboard/notice_details/notice-details.html',
    })

    .state('user-login',{
      url:'/login',
      templateUrl:'stardocx/dashboard/starting_page/starting-page.html'
    })

    .state('user-man-enroll',{
      url:'/user/enroll/man',
      templateUrl:'stardocx/dashboard/user_views/enroll/manual/user-man-enroll.html'
    })

    .state('user-auto-enroll',{
      url:'/user/enroll/auto',
      templateUrl:'stardocx/dashboard/user_views/enroll/auto/user-auto-enroll.html'
    })

    .state('user-profile-edit',{
      url:'/user/profile/edit',
      templateUrl:'stardocx/dashboard/user_views/profile/edit/user-profile-edit.html'
    });

  }

  configureRoutes.$inject = ['$stateProvider','$urlRouterProvider'];

  angular
  .module('app.dashboard')
  .config(configureRoutes);

})();
