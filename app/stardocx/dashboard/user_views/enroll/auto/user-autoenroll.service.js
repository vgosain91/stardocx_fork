(function(){
  'use strict';
  function UserAutoEnrollService($q, DataStore, apiBaseUrl,ResourceService){

    var service={
      registerUser:__registerUser,
      deleteUser:__deleteUser,
    }

    function __registerUser(enrollmentNo,userData){
      var deferred = $q.defer();
      var userFbInfoData = DataStore.get('userFbInfo');
      var dataStorePermanentToken = DataStore.get('permanentToken');
      if(!dataStorePermanentToken){
        deferred.reject({msg:"login invalid"});
      }
      else{
        var postData = {'enrollmentNumber' : enrollmentNo};
        ResourceService.update(
              {'userId': userFbInfoData.id},
              postData,
              function success(response) {
                deferred.resolve(response);
              },
              function failure(err) {
                deferred.reject(err);
              });
      }
      return deferred.promise;
    }

    function __deleteUser(userId){
      var deferred = $q.defer();
      ResourceService.delete(
            {'userId': userId},
            null,
            function success(response) {
              deferred.resolve(response);
            },
            function failure(err) {
              deferred.reject(err);
            });
       return deferred.promise;
     }
    return service;
    }
    UserAutoEnrollService.$inject=['$q','DataStore','apiBaseUrl','ResourceService'];

    angular
    .module('app.dashboard')
    .factory('userAutoEnrollService',UserAutoEnrollService);
  })();
