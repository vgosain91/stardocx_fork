(function(){
  'use strict';
  function UserAutoEnroll($state,DataStore,userAutoEnrollService,$controller){
    var vm =this;
    //vm.FBLoginObj = $controller('FBLogin');
    //vm.FBLoginObj.authenticateUser('enroll');
    vm.description = "You are almost there. Your fb account has been linked. To get personalized experience, tell us.";
    vm.userData = DataStore.get('userFbInfo');
    vm.champ = vm.userData.picture.data['url'];
    vm.enrollmentNumber = '';


    vm.submitUserInfo=function(){

      var dataStoreFbInfo = DataStore.get('fbInfo');

      if(dataStoreFbInfo && vm.enrollmentNumber){
        userAutoEnrollService
        .registerUser(vm.enrollmentNumber,vm.userData)
        .then(funcSuccess,funcFailure);

        function funcSuccess(result){
          DataStore.set('userId', result.id);
          DataStore.set('permanentToken', result.permanentToken);

          $state.go('notices');
        }

        function funcFailure(err){
          if(err.msg==="login invalid"){
            //show modal for asking user to login again.
            $state.go("starting-page");
          }
        }
      }
    }

  vm.deleteUserInfo = function () {
    userAutoEnrollService
    .deleteUser('1069538533139118')
    .then(funcSuccess,funcFailure);
    function funcSuccess(result){
      console.log('Deleted');
    }

    function funcFailure(error){
    }
  }

  vm.showEnrollDef =function(){
    var msg="Enrollment number is the 11 digit university registration number which also acts for a roll number for a student.";
    vm.open(msg);
  }

}

UserAutoEnroll.$inject = ['$state','DataStore','userAutoEnrollService','$controller'];
angular
.module('app.dashboard')
.controller('UserAutoEnroll',UserAutoEnroll);

})();
