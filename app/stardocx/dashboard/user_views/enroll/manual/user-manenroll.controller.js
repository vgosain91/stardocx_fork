(function(){
  'use strict';
  function UserManEnroll($state, DataStore, userManEnrollService,userProfileEditService){
    var vm =this;
    //  vm.FBLoginObj = $controller('FBLogin');
    //  vm.FBLoginObj.authenticateUser('enroll');
    vm.userInfo = DataStore.get('userFbInfo');
    vm.champ = vm.userInfo.picture.data['url'];
    vm.colleges = userProfileEditService.colleges;
    vm.programs = userProfileEditService.programs;
    vm.userInfo = {
      'first_name' : '',
      'last_name' : '',
      'college' : '',
      'program': '',
      'batch' : '',
      'email' : ''
    }

    vm.submitUserInfo=function(){

      var dataStoreFbInfo = DataStore.get('fbInfo');

      if(dataStoreFbInfo){
        userManEnrollService
        .registerUser(vm.userInfo)
        .then(funcSuccess,funcFailure);

        function funcSuccess(result){
          DataStore.set('userId', result.id);
          $state.go('notices');
        }

        function funcFailure(err){
          if(err.msg==="login invalid"){
            //show modal for asking user to login again.
            $state.go("user-login");
          }
        }
      }
    }

  }

  UserManEnroll.$inject = ['$state','DataStore','userManEnrollService','userProfileEditService'];
  angular
  .module('app.dashboard')
  .controller('UserManEnroll',UserManEnroll);

})();
