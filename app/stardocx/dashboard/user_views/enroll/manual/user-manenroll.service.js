(function(){
  'use strict';
  function userManEnrollService($q, DataStore, apiBaseUrl,ResourceService){

    var service={
      registerUser:__registerUser,
      deleteUser:__deleteUser
    }

    function __registerUser(userInfo){
      var deferred = $q.defer();
      var permanentToken = DataStore.get('permanentToken');
      var userFbInfoData = DataStore.get('userFbInfo');
      if(!permanentToken){
        deferred.reject({msg:"login invalid"});
      }
      else{
        var postData = {
          'first_name' : userInfo['first_name'],
          'last_name' : userInfo['last_name'],
          'college' : userInfo['college'],
          'program' : userInfo['program'],
          'batch' : userInfo['batch'],
          'email' : userInfo['email']
        }

        ResourceService.update(
              {'userId': userFbInfoData.id},
              postData,
              function success(response) {
                deferred.resolve(response);
              },
              function failure(err) {
                deferred.reject(err);
              });
     }
      return deferred.promise;
    }

     function __deleteUser(userId){
       var deferred = $q.defer();
       ResourceService.delete(
             {'userId': userId},
             null,
             function success(response) {
               deferred.resolve(response);
             },
             function failure(err) {
               deferred.reject(err);
             });
        return deferred.promise;
      }


    return service;
  }
    userManEnrollService.$inject=['$q','DataStore','apiBaseUrl','ResourceService'];

    angular
    .module('app.dashboard')
    .factory('userManEnrollService',userManEnrollService);
  })();
