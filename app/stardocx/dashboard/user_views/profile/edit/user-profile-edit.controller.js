(function(){
  'use strict';
  //Controllers should not be dependent on each other for methods. Move the methods in service if this is required
  function UserProfileEdit(DataStore, userProfileEditService, $controller ,$state){
    var vm =this;
    //vm.FBLoginObj = $controller('FBLogin');
    //vm.FBLoginObj.authenticateUser('user-profile-edit');
    vm.userInfo = DataStore.get('userFbInfo');
    vm.champ = vm.userInfo.picture.data['url'];
    vm.enrollmentNumber = '';
    vm.userInfo = {
      'first_name' : '',
      'last_name' : '',
      'college' : '',
      'program': '',
      'batch' : '',
      'email' : '',
      'enrollmentNumber' : ''
    };

    vm.colleges = userProfileEditService.colleges;
    vm.programs = userProfileEditService.programs;

    userProfileEditService
    .getUser()
    .then(funcSuccess,funcFailure);

    function funcSuccess(result){
      if(result){
        vm.userInfo = {
          'first_name' : result['first_name'],
          'last_name' : result['last_name'],
          'college' : result['college'],
          'program' : result['program'],
          'batch' : parseInt(result['batch']),
          'email' : result['email'],
          'enrollmentNumber' : result['enrollmentNumber']
        };
      }
    }

    function funcFailure(err){
    }

  vm.updateEnrollmentNo = function () {

    var dataStoreFbInfo = DataStore.get('fbInfo');
    if(dataStoreFbInfo){
      userProfileEditService
      .updateUserEnrollmentNo(vm.userInfo)
      .then(funcSuccess,funcFailure);

      function funcSuccess(result){
        vm.msg="Enrollment No updated successfully.";
        vm.open(vm.msg);
        DataStore.set('userId', result.id);
      }

      function funcFailure(){
        vm.msg="Your Enrollment No update failed. Kindly try after some time.";
        vm.open(vm.msg);
      }
    }
  }

  vm.updateUserInfo=function(){

    var dataStoreFbInfo = DataStore.get('fbInfo');
    if(dataStoreFbInfo){
      userProfileEditService
      .updateUser(vm.userInfo)
      .then(funcSuccess,funcFailure);

      function funcSuccess(result){
        vm.msg="Account updated successfully.";
        vm.open(vm.msg);
        DataStore.set('userId', result.id);
      }

      function funcFailure(){
        vm.msg="Your account update failed.Kindly try after some time.";
        vm.open(vm.msg);
      }
    }
  }

  vm.doAction =function(){
    $state.go('notices');
  }

}

UserProfileEdit.$inject = ['DataStore','userProfileEditService','$controller','$state'];
angular
.module('app.dashboard')
.controller('UserProfileEdit',UserProfileEdit);

})();
