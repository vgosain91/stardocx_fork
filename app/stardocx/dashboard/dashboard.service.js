(function(){
'use strict';
  function DashboardService($log,$http,apiBaseUrl){

    function _getData(queryParm){
      var api=apiBaseUrl + '/' + queryParm;
      $log.info('Inside get Data');
      return  $http
      .get(api)
      .then(function(response){
        return response.data;
      },function(err){
        $log.info(err);
      });
    }

    var service={
      data:_getData
    };

    return  service;
  }

  DashboardService.$inject = ['$log','$http','apiBaseUrl'];
  angular
  .module('app.dashboard')
  .factory('dashboardService', DashboardService);

})();
