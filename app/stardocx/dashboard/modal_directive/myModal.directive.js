(function(){
  'use strict';
  function myModal(){
    var directive={
      scope:{},
      bindToController:{
        open:'=',
        doaction : '&',
        msg:"="
      },
      controller:'MyModalController as mmc',
      restrict:'E',
      templateUrl:'stardocx/dashboard/modal_directive/modal.html'
    }
    return directive;
  }

  angular
    .module('app.dashboard')
    .directive('myModal',myModal);
})();
