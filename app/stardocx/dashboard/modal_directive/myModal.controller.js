(function(){
  'use strict';
  function myModalController($uibModal){
    var vm =this;

    // $scope.items = ['item1', 'item2', 'item3'];

    vm.animationsEnabled = true;


    vm.open = function (size) {
      var modalInstance = $uibModal.open({
        animation: vm.animationsEnabled,
        templateUrl: 'myModalContent.html',
        controller: 'MyModalInstanceController',
        controllerAs:'mmi',
        size: size,
        resolve: {
          msg: function () {
            return size;
          }
        }
      });

      modalInstance.result.then(function () {
        // $scope.selected = selectedItem;
          vm.doaction();
      }, function () {
        $log.info('Modal dismissed at: ' + new Date());
      });
    };

  vm.toggleAnimation = function () {
      vm.animationsEnabled = !vm.animationsEnabled;
    };
  }

  myModalController.$inject=['$uibModal'];

  angular
    .module('app.dashboard')
    .controller('MyModalController',myModalController);
})();
