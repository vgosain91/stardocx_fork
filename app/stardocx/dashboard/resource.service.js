 (function apiResource() {
   'use strict';
   function ResourceService(apiBaseUrl,$resource) {
     var resource =  $resource(apiBaseUrl+'/user/:userId',null,{
                           update : {
                             method : 'PUT'
                           },
                           delete : {
                             method : 'DELETE'
                           },
                           save : {
                             method : 'POST'
                           }
                         });
      return resource;
   }

   ResourceService.$inject = ['apiBaseUrl','$resource'];
   angular
    .module('app.dashboard')
    .factory('ResourceService',ResourceService);
 })();
