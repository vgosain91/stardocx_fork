(function(){
  'use strict';
  angular
    .module('app.dashboard',['naif.base64'])
    //TODO - Define constants at one place
    //Also defined in service-worker.js and app.js.
    // If any change is made, please change there as well
    .constant('apiBaseUrl','https://api.stardocx.com');
})();
