(function(){
  'use strict';
  function StartingPage($controller, DataStore, $state, startingPageService){
    var vm = this;
    vm.bg="../../../images/background_mobile.png";
    vm.loading = false;
    vm.title = '*StarDocX';
    vm.description = "Create an account with Stardocx today and get access to all college related documents at one place."+
    "With Stardocx you can create list, mark important content as favourite and receive and share important " +
    "notes with your peers.";
    vm.FBLoginObj = $controller('FBLogin');
    //  vm.FBLoginObj.authenticateUser('starting-page');
    vm.Login = function() {
      vm.FBLoginObj.authenticateFBLogin().then(function(result) {
        vm.loading = true;
        startingPageService
        .getPermanentToken()
        .then(funcSuccess,funcfailure);

        function funcSuccess(response){
          if(response.message==="This facebook account is already registered!"){
            $state.go('notices');
          }
          else{
            $state.go('user-auto-enroll',{reload:true});
          }
        }
        function funcfailure(response){
          vm.loading = false;
        }
      });
    }

  }

  StartingPage.$inject = ['$controller','DataStore','$state','startingPageService'];
  angular
  .module('app.dashboard')
  .controller('StartingPage',StartingPage);
})();
