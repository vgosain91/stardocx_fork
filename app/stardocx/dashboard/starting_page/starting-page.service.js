(function(){
'use strict';
function startingPageService(DataStore, apiBaseUrl, $q,ResourceService){
  var service={
    getPermanentToken:__getPermanentToken
  }

  function __getPermanentToken(){
    var deferred =$q.defer();
    var dataStoreFbInfo = DataStore.get("fbInfo");
    var dataStoreUserFbInfo =  DataStore.get("userFbInfo");

    if(dataStoreFbInfo && dataStoreUserFbInfo){
      var userData = dataStoreUserFbInfo;
      var postData = {
        'first_name' : userData['first_name'],
        'last_name': userData['last_name'],
        'gender':userData['gender'],
        'email':userData['email'],
        'picture':userData.picture.data.url
      };
      ResourceService.save(
        {'userId': null},
        postData,
        function success(response) {
          DataStore.set("permanentToken", response.permanentToken),
          deferred.resolve(response);
        },
        function failure(error) {
          deferred.reject({response:"OOps,something wrong happened ,kindly refresh the page and try again later."});
        }
      );
  }
  else{
    deferred.reject({response:"fb login invalid"});
  }
  return deferred.promise;
}
  return service;
}

  startingPageService.$inject=['DataStore', 'apiBaseUrl', '$q', 'ResourceService'];

  angular
    .module('app.dashboard')
    .factory('startingPageService',startingPageService);
})();
