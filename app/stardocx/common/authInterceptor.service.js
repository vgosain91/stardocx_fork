  (function InterceptorIIFE(){
  'use strict';
  function authInterceptor($q, DataStore, $rootScope, USER_EVENT){


    function requestHandler(config){
      var facebookInfo  = DataStore.get('fbInfo');
      if(facebookInfo && facebookInfo.status === 'connected' && facebookInfo.authResponse && facebookInfo.authResponse.accessToken){
        // var token = facebookInfo.authResponse.accessToken;
        var token = DataStore.get('permanentToken');

        //Redundant check. Already checked in above block
        // if(token){
        //   config.headers.Authorization = 'Bearer'+token;
        // }
        if(token){
        config.headers.Authorization = 'Bearer '+token;
        }else{
          config.headers.Authorization = 'Bearer '+ facebookInfo.authResponse.accessToken;
        }
      }
      return config;
    }

    function responseHandler(response){

      return response;
    }

    function responseErrorHandler(response){

      if (response.status === 403) {
        $rootScope.$emit(USER_EVENT.LOGIN_FAIL);
      }
      return $q.reject(response);
    }

    var interceptor = {
      request: requestHandler,
      response: responseHandler,
      responseError: responseErrorHandler,
    };



    return interceptor;
  }

  authInterceptor.$inject = ['$q','DataStore','$rootScope','USER_EVENT'];
  angular.module('app.common')
    .factory('authInterceptor', authInterceptor);
})();
