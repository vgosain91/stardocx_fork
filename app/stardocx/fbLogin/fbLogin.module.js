(function IIFE(){
  'use strict';

  function facebookConfig(ezfbProvider, APP_ID) {
    ezfbProvider.setInitParams({
      appId: APP_ID
    });
  }

  facebookConfig.$inject = ['ezfbProvider', 'APP_ID'];

  angular.module('app.facebook',
    [
      'ezfb'
    ]
  ).constant('APP_ID', 161516367540070)
    .config(facebookConfig);
})();
