// jshint ignore: start
// jscs:disable


(function IIFE(){

  "use strict";
  function FBLogin(ezfb, DataStore, $state, $q, userProfileEditService, $location){
    var vm = this;
    function updateUserFbInfo() {
      var deferred = $q.defer();
      ezfb.api('/me',{fields:'id,first_name,last_name,gender,email,picture.width(1000).height(1000)'}, function(userFbData) {
        DataStore.set('userFbInfo', userFbData);
        //$localStorage.userFbInfo = userFbData;
        deferred.resolve(userFbData);
      });
      return deferred.promise;
    }


    /**
     *
     * Check for login event. If localStorage values not available, send out login error event.
     * When the login error event occurs listen to the event and send the user to the login page.
     *
     *
     */


/*    vm.authenticateUser = function(requestUrl) {
      ezfb.getLoginStatus().then(function(response){
        if(response.status === 'connected' && $localStorage.userFbInfo!=null){                  //valid FB login
          if($localStorage.userId){                           //User Exists in DB and mongoID is present in localStorage
            updateUserFbInfo().then(function(result) {
              console.log('came here');
              debugger;
              if(requestUrl!='enroll' && requestUrl!='starting-page'){
                $state.go(requestUrl,{reload:true});          //send to requested url
              }else{
                $state.go('notices',{reload:true});           //send to notices
              }
            });
          }else{
            userProfileEditService.verifyUser(),.then(function verify(res) {
              console.log(res);
              if(res['existing']===true){                     //User Exists in DB and fbId is present at server end
                updateUserFbInfo().then(function(result) {
                  if(requestUrl!='enroll' && requestUrl!='starting-page'){
                    $state.go(requestUrl,{reload:true});      //send to requested url
                  }else{
                    $state.go('notices',{reload:true});           //send to notices
                  }
                });
              }else{                                          //User is new send user to enroll Page
                updateUserFbInfo().then(function(result) {
                  debugger;
                  $location.path('/user/auto/enroll');
                  $state.transitionTo('user-auto-enroll',{reload:true});
                  // $state.go('user-auto-enroll',{reload:true});
                });
              }
            });
          }
        }else{                                                //FB login required.
          $state.go('starting-page',{reload:true});
        }
      });

      */
      vm.authenticateFBLogin = function(){
        var deferred = $q.defer();
        ezfb.login().then(fbSuccess,fbFailure);


        function fbSuccess(res) {
          //$localStorage.fbInfo = res;
          DataStore.set('fbInfo',res);
          if(res['authResponse']!=null){
            // updateUserFbInfo();
            updateUserFbInfo().then(function(result) {
              deferred.resolve(DataStore.get('userFbInfo'));
            });

            // ezfb.api('/me',{fields:'id,first_name,last_name,gender,email,picture.width(1000).height(1000)'}, function(userData) {
            //   console.log(userData);
            //   $localStorage.userFbInfo = userData;
            //      deferred.resolve(userData);
            //     registerUser(response.authResponse);
            // });
            //deferred.resolve($localStorage.userFbInfo);
          }
          else{
            deferred.reject();
          }
        }

        function fbFailure(res) {
          DataStore.remove('fbInfo');
          DataStore.remove('userFbInfo');

          // $localStorage.fbInfo = null;
          // $localStorage.userFbInfo = null;
          deferred.reject();
        }

        return deferred.promise;

      }

    }
    FBLogin.$inject = ['ezfb', 'DataStore','$state','$q','userProfileEditService','$location'];

    angular
    .module("app.facebook")
    .controller("FBLogin", FBLogin);

  })();
