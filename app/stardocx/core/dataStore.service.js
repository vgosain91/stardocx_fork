(function DataStoreIIFE(){
    'use strict';
    function DataStore($localStorage, $log){

      function _setInStore(key, value){
          try{
            if (typeof key === 'string'){
              $localStorage[key] = value;
            }else{
              throw ('Please pass a string key. Current key passes is - ', key);
            }
          }catch(exception){
            $log.error(exception.message);
          }

      }

      function _getFromStore(key){
          var storeKey = null;
          if(key){
            storeKey = $localStorage[key];
          }
          return storeKey;
      }

      function _deleteFromStore(key){
        _setInStore(key, null);
      }

      var dataStore = {
        set: _setInStore,
        get: _getFromStore,
        remove: _deleteFromStore
      };
      return dataStore;

    }

    DataStore.$inject = ['$localStorage', '$log'];
    angular
      .module('app.core')
      .factory('DataStore', DataStore);


})();
