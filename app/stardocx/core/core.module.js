(function(){
  'use strict';

  //CoreConfig.$inject = ['$rootScope','$state'];
  angular.module('app.core',
    [
      'ngAnimate',
      'ngCookies',
      'ngResource',
      'ngRoute',
      'ngSanitize',
      'ngTouch',
      'ngStorage',
      'ui.bootstrap',
      'ui.select',
      'ui.router'
    ]
  );


})();
