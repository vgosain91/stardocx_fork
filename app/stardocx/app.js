(function(){
  'use strict';
  /**@jsdoc - Bootstrapping all modules for star docX
<<<<<<< HEAD
  *
  */
  function serviceWorker(){
    if ('serviceWorker' in navigator) {
    	console.log('Service worker is supported.');
    	navigator.serviceWorker.register('service-worker.js').then(function(registration) {
    		console.log('Service Worker registration was successful.', registration.scope);
        navigator.serviceWorker.ready.then(function(registration){
          registration.pushManager.subscribe({
      			userVisibleOnly: true
      		}).then(function(sub) {
      			console.log('endpoint:', sub.endpoint);
            idToServer(sub.endpoint);
      		});
        });
    	}).catch(function (err) {
    		console.log('Service Worker registration not successful.', err);
    	});
    }
  }


function idToServer(reg_id){

  reg_id = reg_id.split('/').pop();
  var API_BASE_URL = "https://api.stardocx.com";
  $.ajax({
    url:API_BASE_URL+"/notification",
    type:"POST",
    data:{
      'id': reg_id
    },
    success:function(response){
      console.log(response);
    },
    error:function(response){
      console.log(response);
    }
  });
}



 angular
  .module('starDocXApp',
  [
    'app.core',
    'app.common',
    'app.dashboard',
    'app.directives',
    'app.layout',
    'app.facebook'
  ]
).constant('USER_EVENT',{
   LOGIN_FAIL : 'login-failed',
   LOGIN_SUCCESS_ENROLL_FAIL: 'login-success-enroll-fail',
   LOGIN_SUCCESS: 'login-success'
 })
 .run(serviceWorker);

})();
