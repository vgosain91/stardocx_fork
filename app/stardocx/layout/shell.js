(function() {
    'use strict';
    function Shell($state,DataStore, $scope, $rootScope, USER_EVENT) {
        /*jshint validthis: true */
        var vm = this;
        vm.title = 'StarDocX';

        function _redirectToHomePage(){
          $state.go('notices');
        }

        function _loginFailed(event){
          event.preventDefault();
          DataStore.remove('fbInfo');
          DataStore.remove('userFbInfo');
          $state.go('user-login');
        }

        function validateUser(event, toState, fromState){
          var $fbStoredInfo = DataStore.get('fbInfo');
          var $fbStoredUserInfo = DataStore.get('userFbInfo');
          if (!($fbStoredInfo && $fbStoredInfo.status === 'connected' && $fbStoredInfo.authResponse && $fbStoredInfo.authResponse.accessToken && $fbStoredUserInfo)) {
            if(toState  && fromState   && fromState.name !== toState.name){
              $rootScope.$emit(USER_EVENT.LOGIN_FAIL);
            }
          }else {
            if(toState && toState.name === 'user-login'){
              _redirectToHomePage();
            }
        }

      }

      function urlChangeStart(event, toState, toParams, fromState){
        validateUser(event, toState, fromState);
      }

      var stateStartEvent = $rootScope.$on('$stateChangeSuccess', urlChangeStart);
      $scope.$on('$destroy', stateStartEvent);

      var loginFailEvent = $rootScope.$on(USER_EVENT.LOGIN_FAIL, _loginFailed);
      $scope.$on('$destroy', loginFailEvent);
    }

  Shell.$inject = ['$state', 'DataStore', '$scope','$rootScope','USER_EVENT'];

  angular
    .module('app.layout')
    .controller('Shell', Shell);


})();
