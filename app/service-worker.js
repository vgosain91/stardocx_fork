


//name of the custom cache we are going to use.
//name has to modified using postmessage during production.

var CacheName="https://stardocx.com_v_0.0.4";
var API_BASE_URL = "https://api.stardocx.com";
//files to be cached.
var urlsToCache=[

];



console.log('Started', self);
self.addEventListener('install', function(event) {
  self.skipWaiting();
  console.log('Installed', event);
});
self.addEventListener('activate', function(event) {
  console.log('Activated', event);
});

self.addEventListener('push', function (event) {
	event.waitUntil(
		// TODO Url below has to replaced with something on our server.
		fetch(API_BASE_URL + '/notification').then(function(response) {
			console.log('Response', response);
			return response.json().then(function (data) {
				return self.registration.showNotification(data.title, {
					body: data.message,
					icon: 'images/icon.png',
					tag: data.tags
				});
			});
		}));
});

self.addEventListener('notificationclick', function(event) {
    console.log('Notification click: tag ', event.notification.tag);
    event.notification.close();
    var url = 'http://localhost:9000/#/notices';
    event.waitUntil(
        clients.matchAll({
            type: 'window'
        })
        .then(function(windowClients) {
            for (var i = 0; i < windowClients.length; i++) {
                var client = windowClients[i];
                if (client.url === url && 'focus' in client) {
                    return client.focus();
                }
            }
            if (clients.openWindow) {
                return clients.openWindow(url);
            }
        })
    );
});








/// code to add files to cache
self.addEventListener('install',function(event){
  event.waitUntil(
    caches.open(CacheName)
      .then(function(cache){
      console.log('cache opened.');
    return cache.addAll(urlsToCache);
    })
  );
});

//code to decide whether the data for which a request call is made  by the app is already in the cache or else network is to be used.
/*
 self.addEventListener('fetch',function(event){
  event.respondWith(
    caches.match(event.request)
    .then(function(response){
      if(response){
        return response;
      }
      var fetchRequest= event.request.clone();
      return fetch(fetchRequest)
      .then(function(response){
        //check for a valid response
        if( !response || response.status !== 200 ){
          return response;
        }
        var responseToCache =response.clone();
        caches.open(CacheName).then(function(cache){
          cache.put(event.request,responseToCache);
        });
        return response;
      });
    })
  );
});

*/
//code to delete unwanted old cache when serviceWorker is updated.
self.addEventListener('activate',function(event){
  event.waitUntil(
    caches.keys().then(function(cacheNames){
      return Promise.all(
        cacheNames.map(function(cacheName){
          if(CacheName.indexOf(cacheName) === -1){
            return caches.delete(cacheName);
          }
        })
      );
    })
  );
});
